import Config from './config';

const HTTPService = {
    getCategories() {
        return new Promise((resolve, reject) => {
            fetch(`${Config.serverUrl}categories`)
                .then(response => response.json())
                .then((categories) => {
                    categories = categories.filter((obj) => obj.enabled)
                    resolve(categories)
                })
                .catch((error) => {
                    console.log('error', error);
                    reject(error)
                });
        })
    },

    getBanners() {
        return new Promise((resolve, reject) => {
            fetch(`${Config.serverUrl}banners`)
                .then(response => response.json())
                .then((carouselImages) => {
                    carouselImages = carouselImages.filter((obj) => obj.isActive)
                    resolve(carouselImages)
                })
                .catch((error) => {
                    console.log('error', error);
                    reject(error)
                })
        })
    },

    getProducts() {
        return new Promise((resolve, reject) => {
            fetch(`${Config.serverUrl}products`)
                .then(response => response.json())
                .then((products) => {
                    // carouselImages = carouselImages.filter((obj) => obj.isActive)
                    resolve(products)
                })
                .catch((error) => {
                    console.log('error', error);
                    reject(error)
                })
        })
    }
}
export default HTTPService;