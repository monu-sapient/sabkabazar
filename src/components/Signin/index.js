import React, { Component } from 'react';
import './login.scss';

class Signin extends Component {
    render() {
        return (
            <section className="container-fluid login">
                <div className="row" role="row">
                    <div className="col-md-5 section_left" role="cell">
                        <h1>Login</h1>
                        <h2>Get access to your Orders, Wishlist and Recommendations</h2>
                    </div>
                    <div className="col-md-4 section_right" role="cell">
                         <form>
                            <div className="form-group">
                                <input type="email" id="email" name="email" required="required"  placeholder="Email"/>
                                <label htmlFor="email" className="control-label">Email</label>
                            </div>
                            <div className="form-group">
                                <input type="password" id="password" name="password" required="required"  placeholder="Password"/>
                                <label htmlFor="password" className="control-label">Password</label>
                            </div>
                            <button type="submit" className="button">
                                <span>Login</span>
                            </button>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}

export default Signin;
