import React, { Component } from 'react';
import Carousel from './carousel';
import HTTPService from '../../RequestService';
import './home.scss';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            carouselImages: [],
            categories : []
        }
    }

    async componentDidMount() {
        try {
            let categories = await HTTPService.getCategories(),
                carouselImages = await HTTPService.getBanners();

            this.setState({
                categories,
                carouselImages
            })
        } catch(error) {
            console.log('[Error Getting Result]', error)
        }   
    }

    render() {
        let { carouselImages, categories } = this.state
        return (
            <section className="container-fluid homesection ">
                <div role="row" className="row mb-2 flex-column">
                    <Carousel
                        carouselImages={carouselImages}
                        id="offerCarousel"
                    />
                    <div className="homesection_boader-bottom"></div>
                </div>
                {
                    categories.map((catObj, index) => {
                        catObj.imageUrl = catObj.imageUrl || ""
                        catObj.imageUrl = catObj.imageUrl.replace("/static", "");
                        let classToAdd = index % 2 === 1 ? 'reverse' : ''
                        return (
                            <div role="row" className="row mb-2 flex-column" key={index}>
                                <div className={`homesection_category d-flex flex-row align-items-center text-align-center ${classToAdd}`}>
                                    <div role="cell" className="col">
                                        <img src={catObj.imageUrl} title={catObj.name} alt={catObj.name}/>
                                    </div>
                                    <div role="cell" className="col">
                                        <h5 className="cat-name">{catObj.name}</h5>
                                        <p>{catObj.description}</p>
                                        <button className="button" type="button"><span>Explore {catObj.key}</span></button>
                                    </div>
                                </div>
                                <div className="homesection_boader-bottom"></div>
                            </div>
                        )
                    })
                }
            </section >
        );
    }
}

export default Home;
