import React from 'react';

const Carousel = (props) => {

    let { carouselImages = [], id } = props
    return (
        <div id={id} className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
                {
                    carouselImages.map((ele, index) => {
                        return <li key={index} data-target={`#${id}`} data-slide-to={index} className={index === 0 ? 'active' : ''}></li>
                    })
                }
            </ol>
            <div className="carousel-inner">
                {
                    carouselImages.map((ele, index) => {
                        ele.bannerImageUrl = ele.bannerImageUrl.replace('/static', '')
                        return <div key={index} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
                            <img className="d-block w-100" src={ele.bannerImageUrl} alt={ele.bannerImageAlt} title={ele.bannerImageAlt} />
                        </div>
                    })
                }
            </div>
            <a className="carousel-control-prev d-none d-sm-flex" href={`#${id}`} role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true">Prev</span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next d-none d-sm-flex" href={`#${id}`} role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true">Next</span>
                <span className="sr-only">Next</span>
            </a>
        </div>
    );
}

export default Carousel;
