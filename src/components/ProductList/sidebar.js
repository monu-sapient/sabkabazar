import React from 'react';
const SideBar = (props) => {

    let { categories = [] } = props;
    return (
        <nav aria-labelledby="sidebar" className="navbar navbar-expand-sm col-xs-12 col-sm-4 col-md-3 col-lg-2" role="cell">
            <button className="button navbar-toggler" type="button" data-toggle="collapse" data-target="#sideBarList" aria-controls="sideBarList" aria-expanded="false" aria-label="Toggle Sidebar">
                <span>Select Category</span>
                <i className="arrow down"></i>
            </button>
            <div className="collapse navbar-collapse " id="sideBarList">
                <ul role="menubar" aria-label="mainmenubar" className="navbar-nav" id="mainmenu">
                    {
                        categories && categories.length > 0 && categories.map((catObj, index) => {
                            return (
                                <li role="none" key={`category-menu-item-${index}`} className="nav-item">{catObj.name}</li>
                            )
                        })
                    }
                </ul>
            </div>
        </nav>
    );
}

export default SideBar;
