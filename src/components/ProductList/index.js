import React, { Component } from 'react';
import SideBar from './sidebar';
import HTTPService from '../../RequestService';

import './productlist.scss';

const BuyNow = (props) => { 
    return (
        <div className={`${props.className}`}>  
            <span className="d-none d-xl-inline d-lg-inline align-self-center">MRP Rs. {props.price}</span>
            <button type="button" className={`button buy-now-button`}>
                <span>Buy Now</span>
                <span className="d-xl-none d-lg-none"> @ MRP Rs. {props.price}</span>
            </button>
        </div>    
    )
}

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            productList: []
        }
    }
    async componentDidMount() {
        try {
            let categories = await HTTPService.getCategories();
            let productList = await HTTPService.getProducts();
            this.setState({
                categories,
                productList
            })
        } catch(error) {
            console.log('[Error Getting Result]', error)
        }
    }
    render() {
        let { productList } = this.state;

        return (
            <section className="container-fluid productlist">
                <div className="row sidebar" role="row">
                    <SideBar categories={this.state.categories}/>
                    <div role="cell" className="col-xs-12 col-sm-8 col-md-9 col-lg-10 p-0 productlist-section">
                        <div className="row p-0 m-0">
                            {
                                productList && productList.length > 0 && productList.map((product, index) => {
                                    product.imageURL = product.imageURL.replace('/static', '')
                                    return (
                                            <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3 mt-3 product-block" key={`product-${index}`}>
                                                <h4 className="product-name">{product.name}</h4>
                                                <div className="row m-0 pb-3 product-sepration">
                                                    <div className="col pl-0 align-self-center">
                                                        <img className="product-image" src={product.imageURL} alt={product.name} title={product.name}/>
                                                    </div>    
                                                    <div className="col p-0 d-flex justify-content-between flex-column">
                                                        <p>{product.description}</p>
                                                        <BuyNow price={product.price} className={"d-md-none"}/>
                                                    </div>  
                                                </div>
                                                <BuyNow price={product.price} className={"d-none d-md-flex justify-content-between mb-3"}/>
                                                <div className="dashed-border-bottom"></div>
                                            </div> 
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ProductList;
