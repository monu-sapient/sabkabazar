import React, { Component } from 'react';
import '../Signin/login.scss';

class Register extends Component {
    render() {
        return (
            <section className="container-fluid register">
                <div className="row" role="row">
                    <div className="col-md-5 section_left" role="cell">
                        <h1>Signup</h1>
                        <h2>We do not share your personal details with anyone.</h2>
                    </div>
                    <div className="col-md-4 section_right" role="cell">
                         <form>
                            <div className="form-group">
                                <input type="text" id="firstName" name="firstName" required="required"  placeholder="First Name"/>
                                <label htmlFor="firstName" className="control-label">First Name</label>
                            </div>
                            <div className="form-group">
                                <input type="text" id="lastName" name="lastName" required="required"  placeholder="Last Name"/>
                                <label htmlFor="lastName" className="control-label">Last Name</label>
                            </div>
                            <div className="form-group">
                                <input type="email" id="email" name="email" required="required"  placeholder="Email"/>
                                <label htmlFor="email" className="control-label">Email</label>
                            </div>
                            <div className="form-group">
                                <input type="password" id="password" name="password" required="required"  placeholder="Password"/>
                                <label htmlFor="password" className="control-label">Password</label>
                            </div>
                            <div className="form-group">
                                <input type="password" id="confirmPassword" name="confirmPassword" required="required"  placeholder="Confirm Password"/>
                                <label htmlFor="confirmPassword" className="control-label">Confirm Password</label>
                            </div>
                            <button type="submit" className="button">
                                <span>Signup</span>
                            </button>
                        </form>
                    </div>
                </div>
            </section>   
        );
    }
}

export default Register;
