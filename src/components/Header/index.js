import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './header.scss';

class Header extends Component {
    render() {
        return (
            <header className="container-fluid">
                <div className="row" role="row">
                    <div className="col-md-2 col header_logo-wrapper" role="cell">
                        <Link to="/">
                            <img src="/images/logo.png" alt="Sabka Bazaar logo" title="Sabka Bazaar logo" className="header_logo"/>
                        </Link>
                    </div>
                    <nav aria-labelledby="mainmenu" className="navbar navbar-expand-sm navbar-light col-md-2 col" role="cell">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse header_mainmenu-wrapper" id="navbarSupportedContent">
                            <ul role="menubar" aria-label="mainmenubar" className="navbar-nav mr-auto" id="mainmenu">
                                <li role="none" className="nav-item">
                                    <Link to="/" className="nav-link" role="menuitem" title="Go to home">Home</Link>
                                </li>
                                <li role="none" className="nav-item">
                                    <Link to="/plp" role="menuitem" className="nav-link" title="Go to Products list">Products</Link>
                                </li>
                            </ul> 
                        </div>       
                    </nav>
                    <nav aria-labelledby="authmenu" className="col-md-8 col header_authmenu" role="cell">
                        <ul role="menubar" aria-label="authmenubar" className="nav justify-content-end flex-column" id="authmenu">
                            <li role="none" className="nav-item d-none d-sm-block align-self-end">
                                <Link to="/signin" className="auth-link" role="menuitem" title="Go to signin">SignIn</Link>
                                <Link to="/register" className="auth-link" role="menuitem" title="Go to register">Register</Link>
                            </li>
                            <li role="none" className="nav-item align-self-end">
                                <button  role="menuitem" className="header_authmenu-cartbutton">
                                    <img src="/images/cart.svg" width="25" alt="cart"/>
                                    0 items
                                </button>   
                            </li>
                        </ul>
                    </nav>    
                </div>    
            </header>
        );
    }
}

export default Header;
