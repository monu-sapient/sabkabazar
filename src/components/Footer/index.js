import React, { Component } from 'react';
import './footer.scss';

class Footer extends Component {
    render() {
        return (
            <footer className="container-fluid">
                <div role="row" className="row justify-content-center justify-content-xl-start">
                    <p role="cell">Copyright 2011-2018 Sabka Bazaar Grocery Supplies Pvt Ltd</p>
                </div>    
            </footer>
        );
    }
}

export default Footer;
