import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// App component import
import { 
  Header, 
  Footer, 
  Signin,
  Home,
  ProductList,
  Register
} from './components';

// CSS Import
import './scss/main.scss';

// JS Import 
import 'bootstrap/js/dist/button.js';
import 'bootstrap/js/dist/collapse.js';
import 'bootstrap/js/dist/dropdown.js';
import 'bootstrap/js/dist/carousel.js';
// import 'bootstrap/dist/js/bootstrap.min.js';

function App() {
  return (
    <Router>  
        <Header />
        <Switch>
            <Route path="/signin">
              <Signin />
            </Route>
            <Route path="/plp">
              <ProductList />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/">
              <Home />
            </Route>
        </Switch>
        <Footer />
    </Router>    
  );
}

export default App;
